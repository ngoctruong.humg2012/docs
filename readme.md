## Cài đặt laravel chạy trong docker chỉ trong phút mốt.

### Vì sao phải dùng docker thì vui lòng lên mạng đọc :))))

Bước 1: xác định bản Laravel nào dùng bản PHP bao nhỉêu, dùng web server loại nào: có Nginx và Apache2, DB dùng loại nào? cơ bản vậy đã.

Bước 2: Chọn nguyên liệu để xào nấu
Chọn Laravel 8
Chọn PHP theo phiên bản laravel, thường nó ở trong này nè https://laravel.com/docs/8.x/deployment, ở trường hợp này mình sẽ chọn php7.4 
Chọn web server là Nginx vì mình thích nó, và dùng quen nên thấy nó dễ.
DB thì cứ bốc Mysql bản 5 chấm là được
Bước 3: đên đoạn này thì bắt buộc là phải có kiến thức về docker, còn không thì cứ làm theo cho nó chạy rồi  sau học tiếp, nó gọi là học từ ngọn. 

Đầu tiên thì phải tạo cái project laravel 8. cái này thì dễ rồi, làm cái project mới toanh luôn cho dễ. https://laravel.com/docs/8.x/installation
Sau khi mà đã có project laravel 8 mới toanh thì bắt đầu tạo một file Dockerfile trong cùng thư mục dự án.
Cùng bắt đầu xây nhà thôi nào. mở Dockerfile ra cùng đào móng nhà nào.

dòng đầu tiên viết là `FROM php:7.4-fpm` chọn 7.4-fpm để nó có hiệu suất cao nhất nhé
tới dòng thứ 2, `WORKDIR /var/www` WORKDIR để chỉ định thư mục hoạt động trong những câu lệnh tiếp theo bạn nhé

Tiếp theo thì cần phải cài đặt một số thư viện vào cái máy ảo này để nó có thể chạy ngon, các thư viện mình cài sau đây nó là thư viện cơ bản để chạy một ứng dụng laravel, trong thực tế có thể có những yêu cầu cần tới những thư viện đặc biệt phải cài thêm, lúc đó sẽ chủ động thêm sau.
Đây là một số thư viện cơ bản cần có để chạy php7.4 và laravel 8: libfreetype6-dev, libjpeg62-turbo-dev, libpng-dev, libmcrypt-dev, libpng-dev, zlib1g-dev, libxml2-dev,  libzip-dev, libonig-dev, graphviz, curl
ngoài các thư viện trên thì còn có một số extension của php cần cài đặt nữa: gd, pcntl pdo_mysql mbstring

Gòi, bắt đầu cài đặt mấy cái này. viết vào Dockerfile như sau:





```
FROM php:7.4-fpm
WORKDIR /var/www

RUN apt-get update && apt-get install -y \
libfreetype6-dev \
libjpeg62-turbo-dev \
libpng-dev \
libmcrypt-dev \
libpng-dev \
zlib1g-dev \
libxml2-dev \
libzip-dev \
libonig-dev \
graphviz \
curl \
&& docker-php-ext-configure gd --with-freetype --with-jpeg \
&& docker-php-ext-install -j$(nproc) gd \
&& docker-php-ext-install pcntl pdo_mysql mbstring
```
***

Rồi, tới đây thì để ý thấy đoạn `docker-php-ext-configure gd --with-freetype --with-jpeg ` nó hơi khó hiêu, thì cơ bản cài thư viện gd trên docker nó phải vậy `docker-php-ext-install` là câu lệnh để cài extension trong php của docker nó vậy.
Còn mấy câu lệnh phía trên thì là cài đặt bình thường như trong `Linux` thôi, chưa rõ thì có thể google

Tới đây thì tạm dừng với file Dockerfile, chúng ta chuyển sang xây dựng docker-compose một chút nhỉ. vì chúng ta đang chạy với docker-compose cơ mà.
tạo một file `docker-compose.yml` trong thư mục dự án
gõ nội dung sau vào:

```
version: '3'
services:
  app_php:
    container_name: app_php
    build: 
      context: ./
      dockerfile: Dockerfile
    volumes:
      - ./:/var/www
```
để có đoạn này thì nên tìm hiểu về docker-compose rồi vào làm tiếp, cơ bản thì trong phần services, chúng ta đang xây dựng services `app_php`, service này chạy php được build từ Dockerfile như chúng ta tạo vừa rồi.

ở mục volume `- ./:/var/www` 2 vế được chia ra bởi dấu `:` được giải thích như sau: trước dấu : là thư mục của máy tính bạn, trên ví dụ ./ tức là thư mục hiện tại. sau dấu : là thư mục trong máy ảo docker chúng ta đang tạo ra, mục đích nó được tạo ra là để chia sẻ cái đống code của chúng ta sang bên máy ảo, để máy ảo có thể có code mà chạy. chứ không thì có cái nịt.

đến đây bạn hoàn toàn có thể chạy docker-compose up để xem kết quả là nó chạy được rồi. Tuy nhiên mới chỉ chạy được docker không gặp lôi chứ còn app laravel thì chưa nhé.

sau khi chạy docker-compose up -d thì có kết quả sau:

`Recreating app_php ... done`

như vậy là nó chạy rồi, thử dùng lệnh sau xem sao: `docker exec -it app_php bash` nếu mà nó vào được như thế này `root@eed485e03e8d:/var/www#` thì cơ bản là đã vào được máy ảo rồi, gõ tiếp lệnh `ls -al` waooo thấy hết các file code kìa, đó là quyền năng của cái volumes đấy.

Tiếp tục bây giờ viêt thêm vào docker-compose.yml như sau:
```
version: '3'
services:
  app_php:
    container_name: app_php
    build: 
      context: ./
      dockerfile: Dockerfile
    volumes:
      - ./:/var/www
```
```
  app_nginx:
    image: nginx:latest
    container_name: vme_nginx
    volumes:
    - ./:/var/www
    ports:
    - "8181:80"
    - "5433:433"
``` 
cái đoạn được thêm mới là từ app_nginx. cái này là service nginx để tạo web server chạy được app php, image của nginx thì cứ lấy latest nhé.

mục `ports` cũng là được ngăn cách bởi dấu `:` . phía trước dấu `:` là port được tạo ở phía máy tính của chúng ta nó ánh xạ cái port 80 ở bên trong máy ảo. sau này khi chạy app chúng ta sẽ chạy `localhost:8181` rồi nó đi vào máy ảo chạy port 80. Lưu ý hãy chọn cái port bên trái là một port trên máy bạn chưa chạy nhé.

port 443 cũng tương tự, nhưng nó là port của https. bạn tự tìm hiêu thêm

Như vậy là đã có 2 cái service là php và nginx rồi thì không biết đã chạy được chưa ? câu trả lời là chưa. vì hiện tại 2 cái service này đang độc lập không liên quan gì tới nhau. vậy thì để 2 thằng liên quan tới nhau thì phải sử dụng network để nối 2 thằng này lại với nhau.

Viết lại file docker-compose.yml như sau: 

```
version: '3'
services:
  app_php:
    container_name: app_php
    build: 
      context: ./
      dockerfile: Dockerfile
    volumes:
      - ./:/var/www
    networks:
      - app-network
  app_nginx:
    image: nginx:latest
    container_name: app_nginx
    volumes:
    - ./:/var/www
    ports:
    - "8181:80"
    - "1433:433"
    networks:
      - app-network
networks:
  app-network:
    driver: bridge

```

chúng ta có thêm cái mới đó là `networks`: một network có tên là `app-network` được tạo ra chạy với driver là `bridge`. các service được khai báo networks là `app-network` thì sẽ được thông với nhau.

Bây giờ thứ `docker-compose up -d` xong vào chrome gõ localhost:8181 xem nó chạy chưa nhé.

Bùm, xuất hiện dòng `Welcome to nginx!` => à thì ra vấn đề là nginx nó chạy rồi đó, mà nó chưa thể hiểu được là chúng ta đang để code ở đâu sao mà nó hiểu. 

Tới đây thì lại cần thêm một chút kiến thức về nginx rồi đó. còn nếu chưa có cứ làm theo rồi mình học sau, nhưng nên học nhé.

tạo một file theo dường dẫn sau: `docker/nginx/config.conf` trong code của bạn 
ném nội dung sau vào file đó, nội dung này tôi copy ở mục này đó https://laravel.com/docs/8.x/deployment#nginx
 cứ ném vào rồi mình cùng nhau sửa
```
server {
    listen 80;
    listen [::]:80;
    server_name example.com;
    root /srv/example.com/public;
 
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options "nosniff";
 
    index index.php;
 
    charset utf-8;
 
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
 
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
 
    error_page 404 /index.php;
 
    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }
 
    location ~ /\.(?!well-known).* {
        deny all;
    }
}
```

dòng `listen 80;` là nginx nó tạo ra port 80 để mình ánh xạ sang 8181 bên máy mình đã trình bày ở trên. nếu mà sửa port này thì chỗ port trong docker-compose phải sửa tương ứng. thôi cứ để mặc định đi bạn trẻ.

dòng `root /srv/example.com/public;` là chỉ tới thư mục chứa code, à!!! code của chúng ta đang ở /var/www trên máy ảo. thì sửa dòng này thành `root /var/www/public;`

tạm thời sửa như vậy, đến đây thì app vẫn chưa chạy được đâu, vì mình đang tạo file ở trên máy mình chứ có phải config cho nginx trong máy ảo đâu, nhiệm vụ bây giờ là đưa cái file config này vào trong nginx ảo, vậy thì đưa như nào nhỉ? chắc bạn còn nhớ chức năng của volumes, đúng rồi, ánh xạ nó vào trong máy ảo của nginx, vấn đề bây giờ là ánh xạ vào chỗ nào để nó chạy, chỗ này cần có chút kiến thức về nginx, bạn có thể xem tại đây: http://nginx.org/en/docs/beginners_guide.html#:~:text=The%20way%20nginx%20and%20its,%2Flocal%2Fetc%2Fnginx%20.

Nó sẽ ánh xạ vào thư mục `/etc/nginx/conf.d/` giờ cùng sửa nhé 
```
version: '3'
services:
  app_php:
    container_name: app_php
    build: 
      context: ./
      dockerfile: Dockerfile
    volumes:
      - ./:/var/www
    networks:
      - app-network
  app_nginx:
    image: nginx:latest
    container_name: app_nginx
    volumes:
    - ./:/var/www
    - ./docker/nginx/:/etc/nginx/conf.d
    ports:
    - "8181:80"
    - "1433:433"
    networks:
      - app-network
networks:
  app-network:
    driver: bridge
```

gòi đến đây ta chạy `docker-compose down` rồi `docker-compose up` sau đó vào lại `localhost:8181` xem nó chạy được chưa? chạy được nhưng mà là lỗi 503, vậy vấn đề là gì?

để ý file config nginx có cái dòng `fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;` vấn đề là trong cái service nginx thì làm gì có php mà nó chỉ vào như thế, vậy thì phải call sang thăng php kia nó mới chạy đc. sửa nó lại như sau: `fastcgi_pass app_php:9000;` 

Vậy tại sao lại sửa như vậy? vì cổng `9000` là cổng chạy mặc định của php-fpm, fastcgi_pass là giao thức chuyển tiếp yêu cầu của nginx tới một nơi khác, ở đây chúng ta sẽ chuyển sang máy chủ là `app_php:9000`, phần này phải hiểu hơn về nginx. 

chúng ta có file cấu hình như sau:
```
server {
    listen 80;
    listen [::]:80;
    server_name example.com;
    root /var/www/public;
 
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options "nosniff";
 
    index index.php;
 
    charset utf-8;
 
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
 
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
 
    error_page 404 /index.php;
 
    location ~ \.php$ {
        fastcgi_pass app_php:9000;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }
 
    location ~ /\.(?!well-known).* {
        deny all;
    }
}
```


bây giờ thì chạy lại `docker-compose down` và `docker-compose up -d` rồi vào lại trình duyệt để xem. 

Waoooo... có lỗi xảy ra nhưng thực sự nó đã chạy được, lỗi lần này chỉ là chưa cài đặt composer cho dự án, giờ thì làm tiêp thôi. 

Để cài composer thì trong dự án cần thêm thư viện `zip` và `git`

thêm vào Dockerfile 2 thư viện còn thiếu như sau: 
```
FROM php:7.4-fpm
WORKDIR /var/www

RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libmcrypt-dev \
    zlib1g-dev \
    libxml2-dev \
    libzip-dev \
    libonig-dev \
    graphviz \
    curl \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pcntl pdo_mysql mbstring

RUN apt-get install -y \
    git \
    zip
```

Lưu ý: tới đây giải thích vì sao không viết lên trên mà lại viết xuống dưới, vì sau khi thêm vào chúng ta phải chạy lại `docker-compose up --build -d` để build lại và nó sẽ build những đoạn thay đổi mới, vậy nên viết xuống dưới thì đoạn trên sẽ không phải chạy lại => tiêt kiệm thời gian, khi nào có thời gian thì đưa nó lên trên cùng đống kia, lưu ý nếu chỉ chạy `docker-compose up -d` thôi thì sẽ bị cache và đoạn mới thêm sẽ không được build, nên phải thêm `--build` vào.

chạy lệnh `docker-compose up --build -d`

bây giờ cài đặt composer, với mình thì mình hay sử dụng file composer.phar theo phiên bản nhất định, vào đây để xem version https://getcomposer.org/download/

với php7.4 thì nên sử dụng bản 2.0.0, tải file composer.phar rồi ném vào dự án

chạy `docker exec -it app_php bash` để vào máy ảo php sau đó chạy `php composer.phar install`




giờ thì chạy lại `docker exec -it app_php bash` rồi chạy `php composer.phar install` => composer sẽ được cài đặt

Bây giờ vào web thì sẽ ra một đống lỗi liên quan đến permision của file log. nguyên nhân là laravel không có quyền ghi vào file trong ứng dụng, cần phải phân lại quyền cho ông NGINX có thể truy cập file, 

vấn đề phân quyền này là do đang xử dụng volumes ánh xạ thư mục code vào trong máy ảo, mà user bên trong máy ảo lại khác với bên ngoài, nên nó bị lỗi. 

ở ngoài máy chủ host của mình các bạn gõ `id -u` để  lấy cái ID trên máy mình (cái này mình thực hiện trên Ubuntu và Macos còn trên window thì mình k rõ, hình như trên window không cần phân quyền thì phải). hiện tại ID của user máy mình là `1000`.

tiếp theo truy cập vào trong container (`docker exec -it ... bash`) trong này chúng ta sẽ xem các thư mục đang để user là gì, bạn gõ `ls -l` khi đó cột user và group là bao nhiêu, như máy mình nó đang là `1000` (nếu chưa biết cột user group là cột nào thì search google xem phần lệnh ls -l trong linux là nó ra)

giờ đã có cả id user máy chủ host và id container thì tại service app_php trong docker-composer sẽ thêm `user: "1000:1000"` tức là sẽ ánh xạ id trong container vào đúng id user bên máy chủ host.
```
version: '3'
services:
  app_php:
    container_name: app_php
    build: 
      context: ./
      dockerfile: Dockerfile
    volumes:
      - ./:/var/www
    networks:
      - app-network
    user: "1000:1000"
  app_nginx:
    image: nginx:latest
    container_name: app_nginx
    volumes:
    - ./:/var/www
    - ./docker/nginx/:/etc/nginx/conf.d
    ports:
    - "8181:80"
    - "1433:433"
    networks:
      - app-network
networks:
  app-network:
    driver: bridge
```

bây giờ sẽ chạy `docker-compose up --build -d` rồi kiểm tra lại trình duyệt, vậy là chắc chắn được, tuy nhiên khi exec vào container chúng ta sẽ thấy nó hiện kiểu như thế này `I have no name!@425a520bb511:/var/www$` lý do là vì mình đang ánh xạ id mà không có một user cụ thể nào. để đó thì cũng không ảnh hưởng gì, tuy nhiên để cho đẹp ta nên cho nó về với user mặc định là `www-data` đây là user mặc định của `php-fpm`. 

để đưa về user mặc định thì chỉ cần thêm dòng sang vào trong Dockerfile là ok ngay, còn vì sao nó như vậy thì phải tự tìm hiểu thêm

```
RUN usermod -u 1000 www-data
```

giờ thì mọi thứ đã khá là OK,
vào chạy thử thì sẽ còn vài lỗi bên phía laravel do chúng ta chưa chạy hết lệnh, và cái thiếu cuối cùng đó là Database.

Bây giờ sẽ thêm service app_mysql vào docker-compose như sau: 

```
  version: '3'
services:
  app_php:
    container_name: app_php
    build: 
      context: ./
      dockerfile: Dockerfile
    volumes:
      - ./:/var/www
    networks:
      - app-network
    user: "1000:1000"
  app_nginx:
    image: nginx:latest
    container_name: app_nginx
    volumes:
    - ./:/var/www
    - ./docker/nginx/:/etc/nginx/conf.d
    ports:
    - "8181:80"
    - "1433:433"
    networks:
      - app-network
  app_mysql:
    image: mysql:5.7.24
    container_name: app_mysql
    environment:
      MYSQL_DATABASE: laravel
      MYSQL_ROOT_PASSWORD: "1"
      command: mysqld --sql_mode="STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION" --max-allowed-packet=15000000
    volumes:
      - dbmysql:/var/lib/mysql/
    networks:
      - app-network
    ports:
      - "3310:3306"


networks:
  app-network:
    driver: bridge
volumes:
  dbmysql:
    driver: local
```

- ở phần `app_mysql` sử dụng image mysql là phiên bản `5.7.24`, phần biến môi trường `MYSQL_DATABASE` là tên của database, `MYSQL_ROOT_PASSWORD` là password của user `root`, vì user chúng ta sẽ dùng mặc định là root. 
- phần `command` cứ để mặc định như khi copy từ docker-hub. 
- phần `volumes` mình sẽ ánh xạ vào thư mục `/var/lib/mysql` là nơi để lưu lại cái data. nếu như không có phần volumes này thì khi chúng ta chạy `docker-compose down` sẽ bị mất dữ liệu, ở đây sẽ sử dụng volume `dbmysql` được tạo ở phía dưới phần volumes
- `ports` thì mình cũng ánh xạ sang một port mới trên máy chủ
- `networks` thì mình vẫn dùng chung network với các service khác.

- một lưu ý nhỏ là phần volumes cho DB là chúng ta có thể tạo một volume ảo để ánh xạ vào mà không phải sử dụng thư mục như thế kia.

bây giờ mọi thứ đã hoàn chỉnh, chỉ cần chạy `docker-compose up --build -d`  để build lại mọi thứ. dùng `docker-compose ps` để kiểm tra các container trong dự án này đã chạy chưa nhé. hoặc có thể kiểu tra tất cả bằng `docker ps`

- còn một bước cuối là điền các tham số vào trong .env

- `DB_HOST` chúng ta không để là `127.0.0.1` hay `localhost` mà phải để là cái container_name của service mysql, lý do là vì môi trường chạy app đang ở trong docker, nên mọi thứ giao tiếp phải qua tên của các container, => `DB_HOST=127.0.0.1`
- `DB_PORT=3306` chúng ta vẫn lấy port là 3306 vì app đang ở trong docker, khi nào kết nối từ bên ngoài vào trong docker thì mới sử dụng port ánh xạ, ví dụ như sử dụng phần mềm để quản lý DB như Workbench hay Navicat chúng ta sử dụng port ánh xạ, hoặc là khi môi trường php, hay môi trường chạy code nằm ngay trên máy của chúng ta thì sẽ gọi qua port ánh xạ.
- những tham số này đã khai báo ở trong file docker-compose rồi nhé.
```
`DB_DATABASE=laravel`
`DB_USERNAME=root`
`DB_PASSWORD=1`
```
